import { MichelsonMap } from '@taquito/michelson-encoder';
import { compileContract } from './common/compile';
import { runOperation } from './common/tezos-toolkit';
import { getBytes, run } from './common/utils';

run(async tezos => {
    let contractCode = await compileContract('src/ligo/src/fa2_single_asset.mligo', 'single_asset_main');
    let storage = {
        admin: {
            admin: process.env.ADMIN_ADDRESS,
            pending_admin: null,
            paused: false,    
        },
        assets: {
            ledger: new MichelsonMap(),
            operators: new MichelsonMap(),
            token_metadata: MichelsonMap.fromLiteral({
                "0":{
                    token_id: 0,
                    token_info: MichelsonMap.fromLiteral({
                        symbol: getBytes('MSX'),
                        name: getBytes('Masoxium'),
                        decimals: getBytes('10')
                    })
                }
            }),
            total_supply: 0,
        },
        metadata: new MichelsonMap()
    };

    let originateOp = await runOperation(
        async () => await tezos.contract.originate({
            code: contractCode,
            storage,
            balance: '1000',
        })
    );
    console.log(`Deployed: ${originateOp.contractAddress}`);
    console.log(`All successful.`);
});


